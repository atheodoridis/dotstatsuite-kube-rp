# KEYCLOAK

Keycloak is not a simple service as it requires several components to properly work.  
To ease the setup of keycloak, we rely on a [helm](https://helm.sh/) which is a package manager for kubernetes.

There are 2 charts for keycloak, we rely on the [codecentric](https://codecentric.github.io/helm-charts) chart.

## usesul cmds
- `helm upgrade keycloak --atomic --reuse-values --set image.tag=develop codecentric/keycloak -n qa`

## mandatory steps
- add codecentric repo: `helm repo add codecentric https://codecentric.github.io/helm-charts`
- check repo list: `helm repo list`
- check installed chart: `helm list -n <namespace>`
- remove release if needed: `helm delete keycloak -n <namespace>`
- install chart: `helm install keycloak -f values.yaml codecentric/keycloak -n <namespace>`
- upgrade chart (new chart or values change, not for major change): `helm upgrade keycloak -f values.yaml codecentric/keycloak -n <namespace>`

## testing keycloak without https (disable ssl)
- setup postgresql client: `kubectl run postgresql-postgresql-client --rm --tty -i --restart='Never' --namespace <namespace> --image bitnami/postgresql  --env="PGPASSWORD=<password>" --command -- psql --host keycloak-postgresql -U keycloak`
- run the psql cmd: `update REALM set ssl_required = 'NONE' where id = 'master';`

## migration from 11.0.2 to 12.0.4
- `kubectl exec -it -n qa keycloak-0 -- bash`
- `/opt/jboss/keycloak/bin/standalone.sh -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.file=/tmp/qa-export.json.json -Dkeycloak.migration.realmName=OECD -Dkeycloak.migration.usersExportStrategy=REALM_FILE -Djboss.socket.binding.port-offset=99`
- `kubectl cp -n qa keycloak-0:/tmp/qa-export.json qa-export.json`
- `kubectl create configmap -n qa auth-import --from-file=import.json=./qa-export.json`
- edit values-init.yml to handle import if needed
- `helm delete keycloak -n qa`
- `kubectl delete pvc -n qa data-keycloak-postgresql-0`
- `helm install keycloak -f values-init.yaml codecentric/keycloak -n qa`
- remove temporary configmap
- snapshot schedule to update (new disk)

## migration from 7.0.0 to 11.0.2
0. redo step 1, 2 & 3 from *migration from 6.0.1 to 7.0.0*
3. create temporary configmap with the qa-export.json file: `kubectl create configmap -n <namespace> auth-import --from-file=import.json=./qa-export.json`
1. edit values.yml to replace password placeholders (avoid $ and any other char that can be interpolated): `<password>`
3. edit values.yml to handle import if needed
4. run `helm install keycloak -f values.yaml codecentric/keycloak -n <namespace>`
5. make sure the postgresql password is right: `echo $(kubectl get secret -n <namespace> keycloak-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)`
6. wait few minutes for keycloak to start and ingress to catch-up
7. remove temporary configmap
7. remove import for values.yml (to avoid issues if kube restart keycloak)
8. replace password with placeholder
9. **check in incognito mode on browser to avoid cookie issue and too many redirect error**

## migration from 6.0.1 to 7.0.0

0. enter the pod (qa): `kubectl exec -it -n qa keycloak-0 -- bash`
1. export to /opt/jboss/keycloak/bin/qa-export.json: `/opt/jboss/keycloak/bin/standalone.sh -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.file=qa-export.json -Dkeycloak.migration.realmName=OECD -Dkeycloak.migration.usersExportStrategy=REALM_FILE -Djboss.http.port=8888 -Djboss.https.port=9999 -Djboss.management.http.port=7777`
2. exit container: `exit`
3. export from keycloak container (qa) to .: `kubectl cp -n qa keycloak-0:/opt/jboss/qa-export.json qa-export.json`
4. import to keycloak container (staging): `kubectl cp -n staging qa-export.json keycloak-0:/opt/jboss/keycloak/bin/qa-export.json`
5. dive in keycloak container (qa): `kubectl exec -it -n staging keycloak-0 -- bash`
6. import: `cd /opt/jboss/keycloak/bin && ./standalone.sh \
  -Dkeycloak.migration.action=import \
  -Dkeycloak.migration.provider=singleFile \
  -Dkeycloak.migration.file=qa-export.json \
  -Dkeycloak.migration.usersExportStrategy=REALM_FILE \
  -Djboss.http.port=8888 \
  -Djboss.https.port=9999 \
  -Djboss.management.http.port=7777`
7. restart: `kubectl delete pod keycloak-0 -n staging`
8. delete existing helm: `helm delete keycloak -n staging`
9. bump keycloak in values or do nothing (if tag is latest and pull policy is always)
10. install helm: `helm install keycloak -f keycloak/values.yaml codecentric/keycloak -n staging`
11. check proper image: `kubectl describe pod keycloak-0 -n staging`
