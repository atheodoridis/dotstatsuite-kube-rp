## Install redis high availability with helm

# dive in
```
gcloud config set project oecd-228113
gcloud config set container/cluster oecd
gcloud config set compute/region europe-west1
gcloud container clusters get-credentials oecd --region europe-west1-b
kubectl get pods -n staging -o wide
```

# setup
1. kubectl create -f namespace.json
1. create docker regcred secret from `regcred.sh`
1. create secrets from `secrets.sh`

> NO KEYCLOAK IN STAGING because qa frontend use staging backend, keycloak is in-between

## keycloak with helm
> see keycloak.md (at root level)

# solr
1. apply solr init `kubectl apply -f solr.init.yaml`
1. enter the container `kubectl exec -it solr-<hash> -n qa -- bash`
1. create the core `solr create sdmx-facet-search`
1. move the solr file `mv opt/solr/server/solr/*  /data`
1. note: /data should contains files and at least solr.xml
1. remove the worker
1. apply solr `kubectl apply -f solr.yaml`

## redis high availability with helm

- `$ kubectl apply -f redis.yml -n staging`
- remove redis workload
- `$ helm install --name redis -n staging stable/redis-ha`

## Backup Redis

* create snapshot + schedule based on redis disks in Compute Engine

## Restore Redis

* create a disk from snapshot in Compute Engine
* attach GCP disk to a  pod (see redis-bkp.yaml)
* stop redis-ha
* rename /data/dump.rdb
* copy redis-bkp:/data/dump.rdb to redis-ha:/data
* restart redis-ha

NB: above procedure is for a single redis instance, for redis-ha, we may have to copy dump.rdb to all instances
=> To be tested ....

# sfs
- curl -X DELETE https://sfs-qa.siscc.org/admin/dataflows?api-key=<key> | jq
- curl -X DELETE https://sfs-qa.siscc.org/admin/config?api-key=<key> | jq
- curl -X POST https://sfs-qa.siscc.org/admin/dataflows?api-key=<key> -d {} | jq
- curl -X GET https://sfs-qa.siscc.org/admin/report?api-key=<key> | jq
